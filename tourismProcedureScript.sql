﻿use tourismdata;
delimiter /
create procedure update_entrylist
(IN _eventkey int , IN _userkey int , IN _iscompleted boolean, OUT result int)
    begin
        declare exit handler for sqlexception
        begin
            rollback;
            set result = -1;
        end;
        start transaction;
            if exists(select * from entrylist where eventkey = _eventkey and userkey = _userkey) then
                update entrylist set iscompleted = _iscompleted where eventkey = _eventkey and userkey = _userkey;
            else
                insert into entrylist values(_eventkey, _userkey, _iscompleted);
            end if;
            commit;
            set result = 0;
end /
delimiter ;
use tourismdata;
delimiter |
create trigger event_addTrigger after insert on event 
    for each row begin
        insert into entry(eventkey) values (NEW.eventkey);
    end;
|
create trigger entry_insertTrigger after insert on entrylist
    for each row begin
        if NEW.isCompleted = true then
            update entry set numEntry = numEntry + 1 where eventkey = NEW.eventkey;
            update entry set numCompleted = numCompleted + 1 where eventkey = NEW.eventkey;
        elseif NEW.isCompleted = false then
            update entry set numEntry = numEntry + 1;
        end if;
    end;
|

create trigger entry_updateTrigger before update on entrylist
    for each row begin
        if NEW.isCompleted = true then
            update entry set numCompleted = numCompleted + 1 where eventkey = NEW.eventkey;
        elseif NEW.isCompleted = false then
            update entry set numCompleted = numCompleted - 1 where eventkey = NEW.eventkey;
        end if;
    end;    
|

create trigger entry_deleteTrigger before delete on entrylist
    for each row begin
        if OLD.isCompleted = true then
            update entry set numCompleted = numCompleted - 1 where eventkey = OLD.eventkey;
        end if;
        update entry set numEntry = numEntry - 1 where eventkey = OLD.eventkey;
    end;
|

delimiter ; 
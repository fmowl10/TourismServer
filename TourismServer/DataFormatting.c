﻿#include "DataFormatting.h"
#include "parson.h"
#include "serverData.h"
#include "UserList.h"
#include "getTimetoString.h"
#include <winsock2.h>
#include <mysql.h>
#include <stdio.h>
#include <string.h>

int acceptClient(SOCKET serverSocket, fd_set *set, UserList *userList)
{
	SOCKET clientSocket;
	clientSocket = accept(serverSocket, NULL, NULL);
	if (clientSocket > 0)
	{
		FD_SET(clientSocket, set);
		insertUserData(userList, clientSocket, 0, false);
		return clientSocket;
	}
	else {
		return EXIT_FAILURE;
	}
}

int closeClient(SOCKET clientSocket, fd_set *set, UserList *userList)
{
	closesocket(clientSocket);
	FD_CLR(clientSocket, set);
	deletUserData(userList, findUserData(userList, clientSocket, 0));
	return EXIT_SUCCESS;
}

void invalidJson(char *message, int *jsonSize)
{

	JSON_Value *jsonMessage;
	JSON_Object *jsonMessageObject;
	jsonMessage = json_value_init_object();
	jsonMessageObject = json_value_get_object(jsonMessage);
	json_object_set_string(jsonMessageObject, "respones", "Common Error");
	json_object_set_string(jsonMessageObject, "result", "Invalid Json");
	*jsonSize = json_serialization_size(jsonMessage);
	json_serialize_to_buffer(jsonMessage, message, *jsonSize);
	message[*jsonSize - 1] = '\n';	
#ifdef __DEBUG__
	printf("invalidJson : %s\nsize : %d\n", message, *jsonSize);
#endif
	json_value_free(jsonMessage);
}

ProtocolCode getProtocolCode(JSON_Value *jsonMessage)                          
{
	JSON_Object * jsonMessageObject = json_value_get_object(jsonMessage);
	if (jsonMessage == NULL) return ProtocolError;
	const char * requestTable = json_object_get_string(jsonMessageObject, "request");
	const char * responseTable = json_object_get_string(jsonMessageObject, "response");
#ifdef __DEBUG__
	printf("request : %s\nresponse : %s\n", requestTable, responseTable);
#endif
	if ((requestTable && responseTable) || (requestTable == responseTable))
	{
		return ProtocolError;
	}
	if (requestTable)
	{
		return ProtocolRequest;
	}
	/*if (responseTable)             //사용자가 살아있는지에 대한 질문에 대답을 받기위한 처리
	{
		return Protocol_Response;
	}*/
	return ProtocolError;
}

int responseDataFormatting(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, UserList *userList, int index)    //TODO 전체적인 부분이 get을 처리하기 위해 있다. 이것을 다른 처리를 하기 위해 나누자 
{
	JSON_Object * jsonDataObject = json_value_get_object(jsonData);
	RequestCode Code = getRequestCode(json_object_get_string(jsonDataObject, "request"));
	int result = 0;
	if (Code == Request_Error)
	{
		responseError(jsonData, data, jsonSize);
		return 1;
	}
	else if (ISSIGN(Code))
	{
		result = responseSignRequest(mysql, jsonData, data, jsonSize, Code, userList, index);
	}
	else if (ISDATA(Code) && getUserData(userList, index)->isLogin == true)
	{
		result = responseDataRequest(mysql, jsonData, data, jsonSize, Code, getUserData(userList, index));
	}
	else if (ISENTRY(Code) && getUserData(userList, index)->isLogin == true)
	{
		result = responseEntryRequest(mysql, jsonData, data, jsonSize, getUserData(userList, index));
	}
	else
	{
		responseError(jsonData, data, jsonSize);
		return 1;
	}
	if (result == 1)
	{
		responseError(jsonData, data, jsonSize);
		return 1;
	}
	return 0; 
}

int responseDataRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, RequestCode Code, const UserData *userData)
{
	char query[QUERYLENGTH] = { 0, };
	int result = 0;
	Code = dataQueryGenerator(jsonData, query, Code, userData);
#ifdef __DEBUG__
	printf("Excuted Query : %s\n-----------------\n", query);
#endif
	if (Code != Request_Error)
	{
		result = mysql_query(mysql, query);
		if (result != 0)
		{
			printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
			responseError(jsonData, data, jsonSize);
			return 0;
		}
		switch (Code)
		{
			case Get: result = dataFormattingGetCode(mysql, jsonData, data, jsonSize); break;
			case Post: result = dataFormattingPostCode(mysql, jsonData, data, jsonSize); break;
			case Update: result = dataFormattingUpdateCode(jsonData, data, jsonSize); break;
			case Delete: result = dataFormattingDeleteCode(jsonData, data, jsonSize); break;
		}
		if (result == 1)
		{
			responseError(jsonData, data, jsonSize);
		}
	}
	else
	{
		responseError(jsonData, data, jsonSize);
	}
	return 0;
}

RequestCode dataQueryGenerator(JSON_Value * jsonData, char *query, RequestCode Code, const UserData *userData)  //함수형 포인터 배열을 사용하면 확실히 쉽겠지만 Error를 처리할 함수를 만들기에는 무의미하다. 
{
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	const char * table = json_object_get_string(jsonDataObject, "table");
	if (table == NULL) return Request_Error;
	if (strstr(table, "sign") || strcmp(table, "entry") == 0) return Request_Error;
	switch (Code)                                                                         //정보 요청은 이쪽에서 하고, 회원처리를 다른 쪽으로 가게하자
	{
		case Get :
			if (selectQueryGenerator(jsonData, query, userData) == 1) return Request_Error;
			return Code;
		case Post: 
			if (postQueryGenerator(jsonData, query, userData) == 1) return Request_Error;
			return Code;
		case Update: if (updateQueryGenerator(jsonData, query, userData) == 1) return Request_Error;
			return Code;
		case Delete:
			if (deleteQueryGenerator(jsonData, query, userData) == 1) return Request_Error;
			return Code;
	}

	return Request_Error;
}

char **getFieldNames(MYSQL_RES *res, const my_ulonglong numField)
{
	if (numField == 0 || res == NULL) return NULL;
	MYSQL_FIELD *field;
	char **headers = (char**)malloc(sizeof(char**)*(size_t)numField);
	for (unsigned int i = 0; (field = mysql_fetch_field(res)); i++)
	{
		headers[i] = field->name;
#ifdef __DEBUG__
		printf("Header Name : %s\n", headers[i]);
#endif
	}
	return headers;
}


RequestCode getRequestCode(const char * request)
{
#ifdef __DEBUG__
	printf("Request Code : %s\n", request);
#endif // __DEBUG__

	if (strcmp(request, STRGET) == 0) return Get;
	if (strcmp(request, STRPOST) == 0) return Post;
	if (strcmp(request, STRUPDATE) == 0) return Update;
	if (strcmp(request, STRDELETE) == 0) return Delete;
	if (strcmp(request, STRSIGNUP) == 0) return SignUp;
	if (strcmp(request, STRSIGNIN) == 0) return SignIn;
	if (strcmp(request, STRSIGNOUT) == 0) return SignOut;
	if (strcmp(request, STRENTRY) == 0) return Entry;
	return Request_Error;
}

int selectQueryGenerator(JSON_Value *jsonData,char *query, const UserData *userData)
{
	const char *select = "select";
	strcpy_s(query, QUERYLENGTH, select);
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	JSON_Value * key;
	JSON_Array *kindArray;
	int kindLength = 0;
	const char * requestTable = json_object_get_string(jsonDataObject, "table");
	kindArray = json_object_get_array(jsonDataObject, "kind");
	kindLength = json_array_get_count(kindArray);
	if (kindLength == 0)
	{
		return 1;
	}
	for (int i = 0; i < kindLength; i++)
	{
		const char *columnName = json_array_get_string(kindArray, i);
		if (columnName != NULL)
		{
			if (strcmp("all", columnName) == 0)
			{
				strcat_s(query, QUERYLENGTH, " * ");
				break;
			}
			if (strcmp(columnName, "location") == 0)
			{
				sprintf_s(query, QUERYLENGTH, "%s %s, %s,", query, "latitude", "longitude");
				continue;
			}
			sprintf_s(query, QUERYLENGTH, "%s %s,", query, columnName);
		}
	}
	int lastcommaIndex = strlen(query) - 1;
	query[lastcommaIndex] = '\0';
	sprintf_s(query, QUERYLENGTH, "%s from %s", query, requestTable);
	key = json_object_get_value(jsonDataObject, "key");
	if (key != NULL)
	{
		if (keyWhereQueryGenerator(key, query, userData) != 0) return 1;
	}
	JSON_Array *search = json_object_get_array(jsonDataObject, "search");
	if (search != NULL)
	{
		if (searchWhereQueryGenerator(search, query, userData) != 0) return 1;
	}
	limitindexQueryGenerator(json_object_get_value(jsonDataObject, "index"), json_object_get_value(jsonDataObject, "limit"), query);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRGET, query);
#endif 
	return 0;
}

int keyWhereQueryGenerator(JSON_Value *key, char * query, const UserData *userData)
{
	JSON_Object *keyObject;
	keyObject = json_value_get_object(key);
	if (keyObject != NULL)
	{
		const char * keyname = json_object_get_string(keyObject, "name");
		JSON_Value_Type keyType = json_value_get_type(json_object_get_value(keyObject, "key"));
		if ((keyname != NULL) && (keyType != JSONError))
		{
			if (strstr(query, " where ") == NULL)
			{
				switch (keyType)
				{
					case JSONString: 
						if (strcmp(json_object_get_string(keyObject, "key"), "myuserkey") == 0)
						{
							sprintf_s(query, QUERYLENGTH, "%s where %s = %d", query, keyname, userData->key); break;
						}
						else return 1;
					case JSONNumber: sprintf_s(query, QUERYLENGTH, "%s where %s = %d", query, keyname, (int)json_object_get_number(keyObject, "key")); break;
				}
			} 
			else
			{
				switch (keyType)
				{
					case JSONString:
						if (strcmp(json_object_get_string(keyObject, "key"), "myuserkey") == 0)
						{
							sprintf_s(query, QUERYLENGTH, "%s where %s = %d", query, keyname, userData->key); break;
						}
						else return 1;
					case JSONNumber: sprintf_s(query, QUERYLENGTH, "%s where %s = %d", query, keyname, (int)json_object_get_number(keyObject, "key")); break;
				}
			}
#ifdef __DEBUG__
			printf("%s Query : %s\n-------------------\n", "key where", query);
#endif 
			return 0;
		}
	}
	return 1;
}

int searchWhereQueryGenerator(JSON_Array *search, char *query, const UserData *userData)
{
	if (search == NULL || query == NULL) return 1;
	int searchArraySize = json_array_get_count(search);
	if (searchArraySize == 0) return 1;
	for (int i = 0; i < searchArraySize; i++)
	{

		int likeResult = 0;
		int conjunctionResult = 0;
		JSON_Object *searchItem = json_array_get_object(search, i);
		const char *operator = json_object_get_string(searchItem, "operator");
		const char *columnName = json_object_get_string(searchItem, "columnname");
		const char *conjunction = json_object_get_string(searchItem, "conjunction");
		JSON_Value_Type columnType = json_value_get_type(json_object_get_value(searchItem, "value"));
		if (operator != NULL && columnName != NULL && columnType != JSONError || conjunction != NULL)
		{ //value의 데이터 타입이 string이면 ""으로 감싸라
			if (strcmp(operator, "like") == 0) likeResult = 1;
			if (conjunction != NULL) conjunctionResult = 1;
			if (strstr(query, "where") == NULL)
			{
				if (likeResult)
				{
					sprintf_s(query, QUERYLENGTH, "%s where %s %s \"%%%s%%\"", query, columnName, operator, json_object_get_string(searchItem, "value"));
					continue;
				}
				switch (columnType)
				{
					case JSONString:
						if (strcmp(json_object_get_string(searchItem, "value"), "myuserkey") == 0)
						{
							sprintf_s(query, QUERYLENGTH, "%s where %s %s %d", query, columnName, operator, userData->key); break;
						}
						sprintf_s(query, QUERYLENGTH, "%s where %s %s \"%s\"", query, columnName, operator, json_object_get_string(searchItem, "value"));
						break;
					case JSONNumber:
						sprintf_s(query, QUERYLENGTH, "%s where %s %s %d", query, columnName, operator, (int)json_object_get_number(searchItem, "value"));
						break;
					case JSONBoolean:
						if (json_object_get_boolean(searchItem, "value"))
						{
							sprintf_s(query, QUERYLENGTH, "%s where %s %s %s", query, columnName, operator, "true");
							break;
						}
						else
						{
							sprintf_s(query, QUERYLENGTH, "%s where  %s %s %s", query, columnName, operator,"false");
							break;
						}
				}
			}
			else
			{
				if (conjunction)
				{
					if (!strcmp(conjunction, "and") || !strcmp(conjunction, "or"))
					{
						sprintf_s(query, QUERYLENGTH, "%s %s", query, conjunction);
					}
				}
				else
				{
					sprintf_s(query, QUERYLENGTH, "%s %s", query, "and");
				}
				if (likeResult)
				{
					sprintf_s(query, QUERYLENGTH, "%s %s %s \"%%%s%%\"", query, columnName, operator, json_object_get_string(searchItem, "value"));
					continue;
				}
				switch (columnType)
				{
				case JSONString:
					if (strcmp(json_object_get_string(searchItem, "value"), "myuserkey") == 0)
					{
						sprintf_s(query, QUERYLENGTH, "%s %s %s %d", query, columnName, operator, userData->key); break;
					}
					sprintf_s(query, QUERYLENGTH, "%s %s %s \"%s\"", query, columnName, operator, json_object_get_string(searchItem, "value"));
					break;
				case JSONNumber:
					sprintf_s(query, QUERYLENGTH, "%s %s %s %d", query, columnName, operator, (int)json_object_get_number(searchItem, "value"));
					break;
				case JSONBoolean:
					if (json_object_get_boolean(searchItem, "value"))
					{
						sprintf_s(query, QUERYLENGTH, "%s %s %s %s", query, columnName, operator, "true");
						break;
					}
					else
					{
						sprintf_s(query, QUERYLENGTH, "%s %s %s %s", query, columnName, operator, "false");
						break;
					}
				}
			}
		}
		else
		{
			return 1;
		}
	}
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "where", query);
#endif 
	return 0;
}

int limitindexQueryGenerator(JSON_Value *index, JSON_Value *limit, char *query)
{
	if (limit == NULL && index == NULL) return 0;
	if (limit != NULL)
	{
		if (index != NULL)
		{
			sprintf_s(query, QUERYLENGTH, "%s limit %d, %d", query, (int)json_value_get_number(index), (int)json_value_get_number(limit));
			return 0;
		}
		sprintf_s(query, QUERYLENGTH, "%s limit %d", query, (int)json_value_get_number(limit));
		return 0;
	}
	return 1;
}

int updateQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData)
{
	if (jsonData == NULL || query == NULL) return 1;
	strcpy_s(query, QUERYLENGTH, "update ");
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return 1;
	JSON_Value *keyValue = json_object_get_value(jsonDataObject, "key");
	JSON_Array *search = json_object_get_array(jsonDataObject, "search");
	if (jsonDataObject == NULL) return 1;
	const char *requestTable = json_object_get_string(jsonDataObject, "table");
	if (requestTable == NULL) return 1;
	sprintf_s(query, QUERYLENGTH, "%s %s set ", query, requestTable);
	JSON_Array *kind = json_object_get_array(jsonDataObject, "kind");
	JSON_Array *replace = json_object_get_array(jsonDataObject, "replace");
	if (kind == NULL || replace == NULL) return 1;
	int kindArraySize = json_array_get_count(kind);
	int replaceArraySize = json_array_get_count(replace);
	if (kindArraySize == 0 || replace == 0) return 1;
	const char *columnName;
	JSON_Value_Type columnType;
	for (int i = 0; i < (kindArraySize < replaceArraySize ? replaceArraySize : kindArraySize); i++)
	{
		columnType = JSONError;
		columnName = NULL;
		columnName = json_array_get_string(kind, i);
		if (columnName != NULL)
		{
			if (strcmp(columnName, "all") == 0) return 1;
			if (strcmp(columnName, "location") == 0)
			{
				sprintf_s(query, QUERYLENGTH, "%s %s = \"%s\",", query, "latiude", json_array_get_string(replace, i));
				sprintf_s(query, QUERYLENGTH, "%s %s = \"%s\",", query, "longitude", json_array_get_string(replace, i));
				kindArraySize++;
				i++;
				continue;
			}
			if (strcmp(columnName, "myuserkey") == 0)
			{
				sprintf_s(query, QUERYLENGTH, "%s  %s = %d,", query, "userkey", userData->key); break;
			}
			columnType = json_value_get_type(json_array_get_value(replace, i));
			switch (columnType)
			{
				case JSONString:
					sprintf_s(query, QUERYLENGTH, "%s %s = \"%s\",", query, columnName, json_array_get_string(replace, i));
					break;
				case JSONNumber:
					sprintf_s(query, QUERYLENGTH, "%s %s = %d,", query, columnName, (int)json_array_get_number(replace, i));
					break;
				case JSONBoolean:
					if (json_array_get_boolean(replace, i))
					{
						sprintf_s(query, QUERYLENGTH, "%s %s = %s,", query, columnName, "true");
						break;
					}
					else
					{
						sprintf_s(query, QUERYLENGTH, "%s %s = %s,", query, columnName, "false");
						break;
					}
			}
		}
	}
	query[strlen(query) - 1] = '\0';
	if (keyWhereQueryGenerator(keyValue, query, userData) == 1 && searchWhereQueryGenerator(search, query, userData) == 1) return 1;
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRUPDATE, query);
#endif 
	return 0;
}

int deleteQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData)
{
	if (jsonData == NULL || query == NULL) return 1;
	strcpy_s(query, QUERYLENGTH, "delete ");
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return 1;
	JSON_Value *keyValue = json_object_get_value(jsonDataObject, "key");
	JSON_Array *search = json_object_get_array(jsonDataObject, "search");
	if (jsonDataObject == NULL) return 1;
	const char *requestTable = json_object_get_string(jsonDataObject, "table");
	if (requestTable == NULL) return 1;
	sprintf_s(query, QUERYLENGTH, "%s from %s ", query, requestTable);
	if (keyWhereQueryGenerator(keyValue, query, userData) == 1 && searchWhereQueryGenerator(search, query, userData)  == 1) return 1;
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRDELETE, query);
#endif 
	return 0;
}

int postQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData)
{
	if (jsonData == NULL || query == NULL) return 1;
	strcpy_s(query, QUERYLENGTH, "insert into ");
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return 1;
	const char *requestTable = json_object_get_string(jsonDataObject, "table");
	if (requestTable == NULL) return 1;
	sprintf_s(query, QUERYLENGTH, "%s %s(", query, requestTable);
	JSON_Array *kindArray = json_object_get_array(jsonDataObject, "kind");
	if (kindArray == NULL) return 1;
	int kindArraySize = 0; 
	kindArraySize = json_array_get_count(kindArray);
	if (kindArraySize == 0) return 1;
	JSON_Array *postDataArray = json_object_get_array(jsonDataObject, "postdatas");
	if (postDataArray == NULL) return 1;
	int postDatasArraySize = 0;
	postDatasArraySize = json_array_get_count(postDataArray);
	if (postDatasArraySize == 0) return 1;
	const char *columnName;
	int isLocationExist = 0;
	JSON_Value_Type dataType = JSONError;
	for (int i = 0; i < kindArraySize; i++)
	{
		columnName = NULL;
		columnName = json_array_get_string(kindArray, i);
		if (columnName != NULL)
		{
				if (strcmp(columnName, "all") == 0) continue;
				if (strcmp(columnName, "location") == 0)
				{
					sprintf_s(query, QUERYLENGTH, "%s %s, %s,", query, "latitude", "longitude");
					isLocationExist = 1;
					continue;
				}
				sprintf_s(query, QUERYLENGTH, "%s %s,", query, columnName);
		}
	}
	if (kindArraySize + isLocationExist != postDatasArraySize) return 1;
	query[strlen(query) - 1] = ')';
	sprintf_s(query, QUERYLENGTH, "%s values(", query);
	for (int i = 0; i < postDatasArraySize; i++)
	{
		dataType = json_value_get_type(json_array_get_value(postDataArray, i));
		if (dataType != JSONError)
		{
			switch (dataType)
			{
				case JSONString :
					if (strcmp(json_array_get_string(postDataArray, i), "myuserkey") == 0)
					{
						sprintf_s(query, QUERYLENGTH, "%s %d,", query, userData->key); break;
					}
					sprintf_s(query, QUERYLENGTH, "%s \"%s\",", query, json_array_get_string(postDataArray, i));
					break;
				case JSONNumber:
					sprintf_s(query, QUERYLENGTH, "%s %d,", query, (int)json_array_get_number(postDataArray, i));
					break;
				case JSONBoolean :
					if (json_array_get_boolean(postDataArray, i))
					{
						sprintf_s(query, QUERYLENGTH, "%s %s,", query,"true");
						break;
					}
					else
					{
						sprintf_s(query, QUERYLENGTH, "%s %s,", query, "false");
						break;
					}
			}
		}
	}
	query[strlen(query) - 1] = ')';
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRPOST, query);
#endif 
	return 0;
}


int dataFormattingGetCode(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize) //json 구조체를 2개를 가지고 한쪽데이터를 이용해서 저장시키자
{
	JSON_Object * jsonDataObject = json_value_get_object(jsonData);
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataTempObject = json_value_get_object(jsonDataTemp);
	MYSQL_RES * res;
	MYSQL_ROW row;
	my_ulonglong numField = 0;
	my_ulonglong numRow = 0;
	my_ulonglong i;
	int index = (int)json_object_get_number(jsonDataObject, "index");
	res = mysql_store_result(mysql);	
	if (res == NULL)
	{
		mysql_free_result(res);
		json_value_free(jsonDataTemp);
		return 1;
	}
	numRow = mysql_num_rows(res);
	numField = mysql_num_fields(res);
	char **fieldNames = getFieldNames(res, numField);
	jsonDataObject = json_value_get_object(jsonData);
	json_object_set_string(jsonDataTempObject, "response", STRGET);
	json_object_set_string(jsonDataTempObject, "table", json_object_get_string(jsonDataObject, "table"));
	json_object_set_string(jsonDataTempObject, "result", "ok");
	JSON_Value *datasValue = json_value_init_array();
	JSON_Array *datasArray = json_value_get_array(datasValue);
	json_object_set_value(jsonDataTempObject, "Datas", datasValue);
	for (i = 0; i < numRow; i++)
	{
		if (json_serialization_size(jsonDataTemp) + 30 >= MAX_BUF)
		{
			json_array_remove(datasArray, json_array_get_count(datasArray) - 1);
			if (index)
			{
				json_object_set_number(jsonDataTempObject, "index", (double)index);
			}
			else
			{
				json_object_set_number(jsonDataTempObject, "index", (double)0);
			}
			json_object_set_number(jsonDataTempObject, "limit", (double)i);
			mysql_free_result(res);
			*jsonSize = json_serialization_size(jsonDataTemp);
			json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
			json_value_free(jsonData);
			json_value_free(jsonDataTemp);
			data[*jsonSize - 1] = '\n';		
			free(fieldNames);
			return 0;
		}
		row = mysql_fetch_row(res);
		JSON_Value *dataValue = json_value_init_object();
		JSON_Object *dataObject = json_value_get_object(dataValue);
		for (my_ulonglong j = 0; j < numField; j++)
		{
			json_object_set_string(dataObject, fieldNames[j], row[j]);
		}
		json_array_append_value(datasArray, dataValue);
	}
	mysql_free_result(res);
	if (index)
	{
		json_object_set_number(jsonDataTempObject, "index", (double)index);
	}
	else
	{
		json_object_set_number(jsonDataTempObject, "index", (double)0);
	}
	json_object_set_number(jsonDataTempObject, "limit", (double)i);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
	free(fieldNames);
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

int dataFormattingDeleteCode(JSON_Value *jsonData, char *data, int *jsonSize)
{
	JSON_Object * jsonDataObject = json_value_get_object(jsonData);
	int requestTableLen = strlen(json_object_get_string(jsonDataObject, "table")) + 1;
	char * requestTable = (char*)malloc(requestTableLen);
	strcpy_s(requestTable, requestTableLen, json_object_get_string(jsonDataObject, "table"));
	json_value_free(jsonData);
	jsonData = json_value_init_object();
	jsonDataObject = json_value_get_object(jsonData);
	json_object_set_string(jsonDataObject, "response", STRDELETE);
	json_object_set_string(jsonDataObject, "table", requestTable);
	json_object_set_string(jsonDataObject, "result", "ok");
	*jsonSize = json_serialization_size(jsonData);
	json_serialize_to_buffer(jsonData, data, *jsonSize);
	json_value_free(jsonData);
	data[*jsonSize - 1] = '\n';
	free(requestTable);
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

int dataFormattingPostCode(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize)
{
	JSON_Object * jsonDataObject = json_value_get_object(jsonData);
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataTempObject = json_value_get_object(jsonDataTemp); 
	JSON_Value *key =NULL;
	JSON_Object *keyObject = NULL;
	jsonDataObject = json_value_get_object(jsonData);
	json_object_set_string(jsonDataTempObject, "response", STRPOST);
	json_object_set_string(jsonDataTempObject, "table", json_object_get_string(jsonDataObject, "table"));
	json_object_set_string(jsonDataTempObject, "result", "ok");
	my_ulonglong id = 0;
	if ((id = mysql_insert_id(mysql)) > 0)
	{
		char keyName[20] = { 0, };
		sprintf_s(keyName, 20, "%skey", json_object_get_string(jsonDataObject, "table"));
		key = json_value_init_object();
		keyObject = json_value_get_object(key);
		json_object_set_string(keyObject, "name", keyName);
		json_object_set_number(keyObject, "key", (double)id);
		json_object_set_value(jsonDataTempObject, "key", key);
	}
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';	
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}	

int dataFormattingUpdateCode(JSON_Value *jsonData, char *data, int *jsonSize)
{
	JSON_Object * jsonDataObject = json_value_get_object(jsonData);
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataTempObject = json_value_get_object(jsonDataTemp);
	jsonDataObject = json_value_get_object(jsonData);
	json_object_set_string(jsonDataTempObject, "response", STRUPDATE);
	json_object_set_string(jsonDataTempObject, "table", json_object_get_string(jsonDataObject, "table"));
	json_object_set_string(jsonDataTempObject, "result", "ok");
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}


int responseSignRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, RequestCode Code , UserList *list, int index)
{
	int result = CommonError;
	if (getUserData(list, index)->isLogin == false)
	{
		switch (Code)
		{
		case SignUp:result = responseSignUp(mysql, jsonData); break;
		case SignIn: result = responseSignIn(mysql, jsonData, list, index); break;
		}
	}
	else
	{
		if (Code == SignOut)
		{
			result = responseSignOut(mysql, jsonData, list, index);
		}
	}
	json_value_free(jsonData);
	jsonData = json_value_init_object();
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	switch (Code)
	{
		case SignUp: json_object_set_string(jsonDataObject, "response", STRSIGNUP); break;
		case SignIn: json_object_set_string(jsonDataObject, "response", STRSIGNIN); break;
		case SignOut:json_object_set_string(jsonDataObject, "response", STRSIGNOUT); break;
	}
	switch (result)
	{
	case OK: json_object_set_string(jsonDataObject, "result", STROK); break;
		case AlreadyExist_ID_OR_NICKNAME : json_object_set_string(jsonDataObject, "result", STRAlreadyExist_ID_OR_NICKNAME ); break;
		case WRONG_ID_OR_PW: json_object_set_string(jsonDataObject, "result", STRWRONG_ID_OR_PW); break;
		case CommonError : 
		case UnknownError : json_object_set_string(jsonDataObject, "result", STRNOT_OK); break;
	}
	*jsonSize = json_serialization_size(jsonData);
	json_serialize_to_buffer(jsonData, data, *jsonSize);
	json_value_free(jsonData);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}


SignProcessingCode responseSignUp(MYSQL *mysql, JSON_Value *jsonData) //쿼리문 수정 및 로직 수정
{
	if (mysql == NULL || jsonData == NULL) return CommonError;
	MYSQL_RES *res;
	MYSQL_ROW row;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return CommonError;
	const char * id = json_object_get_string(jsonDataObject, "id");
	const char * pw = json_object_get_string(jsonDataObject, "pw");
	const char * nickName = json_object_get_string(jsonDataObject, "nickname");
	if (id == NULL || pw == NULL || nickName == NULL) return CommonError;
	const char * duplicateCheckQuery = "select count(*) from userlist where ID = \"%s\" or nickname = \"%s\"";
	const char * addMemberQuery = "insert into userlist (ID, PW, nickname) values(\"%s\", \"%s\", \"%s\")";
	char query[QUERYLENGTH] = { 0, };
	sprintf_s(query, QUERYLENGTH,duplicateCheckQuery, id, nickName);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRSIGNUP, query);
#endif 
	if (mysql_query(mysql, query) > 0) return UnknownError;
	res = mysql_store_result(mysql);
	row = mysql_fetch_row(res);
	if (atoi(row[0]) != 0)
	{
		mysql_free_result(res);
		return AlreadyExist_ID_OR_NICKNAME;
	}
	sprintf_s(query, QUERYLENGTH, addMemberQuery, id, pw, nickName);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRSIGNUP, query);
#endif 
	if (mysql_query(mysql, query) > 0) return UnknownError;
	mysql_free_result(res);
	return OK;
}

SignProcessingCode responseSignIn(MYSQL *mysql, JSON_Value *jsonData, UserList *list, int index) 
{
	if (mysql == NULL || jsonData == NULL || list == NULL || (index < 0 && index > 1023)) return CommonError;
	MYSQL_RES *res;
	MYSQL_ROW row;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return 1;
	const char * id = json_object_get_string(jsonDataObject, "id");
	const char * pw = json_object_get_string(jsonDataObject, "pw");
	if (id == NULL || pw == NULL ) return 1;
	const char * lookupMemberQuery = "select userkey from userlist where ID = \"%s\" and PW = \"%s\"";
	const char * userlogQuery = "insert into userlog (userkey, date, isLogin) values(%s, \"%s\", true)";
	char query[QUERYLENGTH] = { 0, };
	sprintf_s(query, QUERYLENGTH, lookupMemberQuery, id, pw);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRSIGNIN, query);
#endif 
	if (mysql_query(mysql, query) > 0) return UnknownError;
	res = mysql_store_result(mysql);
	row = mysql_fetch_row(res);
	if (row == NULL)
	{
		mysql_free_result(res);
		return WRONG_ID_OR_PW;
	}
	else 
	{
		setUserData(list, index, 0, atoi((char *)row[0]), true);
		mysql_free_result(res);
		sprintf_s(query, QUERYLENGTH, userlogQuery, row[0], getTimetoString());
#ifdef __DEBUG__
		printf("%s Query : %s\n-------------------\n", STRSIGNIN, query);
#endif 
		mysql_query(mysql, query);
		return OK;
	}
	mysql_free_result(res);
	return UnknownError;
}

SignProcessingCode responseSignOut(MYSQL *mysql, JSON_Value *jsonData, UserList *list, int index)
{
	if (mysql == NULL || jsonData == NULL || list == NULL || (index < 0 && index > 1023)) return CommonError;
	MYSQL_RES *res;
	MYSQL_ROW row;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	if (jsonDataObject == NULL) return 1;
	const char * id = json_object_get_string(jsonDataObject, "id");
	if (id == NULL) return 1;
	const char * lookupMemberQuery = "select userkey from userlist where ID = \"%s\"";
	const char * userlogQuery = "insert into userlog (userkey, date, isLogin) values(%s, \"%s\", false)";
	char query[QUERYLENGTH] = { 0, };
	sprintf_s(query, QUERYLENGTH, lookupMemberQuery, id);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", STRSIGNOUT, query);
#endif 
	if (mysql_query(mysql, query) > 0) return UnknownError;
	res = mysql_store_result(mysql);
	if (res == NULL)
	{
		mysql_free_result(res);
		return WRONG_ID_OR_PW;
	}
	else
	{
		row = mysql_fetch_row(res);
		setUserData(list, index, 0, 0, 0);
		sprintf_s(query, QUERYLENGTH, userlogQuery, row[0], getTimetoString());
#ifdef __DEBUG__
		printf("%s Query : %s\n-------------------\n", STRSIGNOUT, query);
#endif 
		mysql_query(mysql, query);
		mysql_free_result(res);
		return OK;
	}
	mysql_free_result(res);
	return UnknownError;
}


int responseEntryRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	int result = 1;
	if (json_object_has_value_of_type(jsonDataObject, "findevent", JSONString))
	{
		result = responseFindEvent(mysql, jsonData, data, jsonSize, userData);
	}
	else if (json_object_has_value_of_type(jsonDataObject, "iscompleted", JSONBoolean))
	{
		result = responseIsCompleted(mysql, jsonData, data, jsonSize, userData);
	}
	else if (json_object_has_value_of_type(jsonDataObject, "attend", JSONBoolean))
	{
		result = responseAttend(mysql, jsonData, data, jsonSize, userData);
	}
	else if (json_object_has_value_of_type(jsonDataObject, "checkentry", JSONString))
	{
		result = responseCheckEntry(mysql, jsonData, data, jsonSize, userData);
	}
	else if (json_object_has_value_of_type(jsonDataObject, "getentrylist", JSONString))
	{
		result = responseGetEntryList(mysql, jsonData, data, jsonSize, userData);
	}
	else
	{
		result = responseError(jsonData, data, jsonSize);
	}
	if (result)
	{
		responseError(jsonData, data, jsonSize);
	}
	return 0;
}

int responseError(JSON_Value *jsonData, char *data, int *jsonSize)
{
	if (jsonData == NULL || data == NULL || jsonSize == 0) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STRNOT_OK);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}
int responseFindEvent(MYSQL * mysql, JSON_Value * jsonData, char * data, int * jsonSize, const UserData * userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	const char *findEventKeyOption = json_object_get_string(jsonDataObject, "findevent");
	if (findEventKeyOption == NULL) return 1;
	if (json_object_has_value_of_type(jsonDataObject, "kind", JSONArray) == 0) return 1;
	EventKeyOption option = getEventKeyOption(findEventKeyOption);
	if (option == EventKeyOption_Error) return 1;
	const char * queryFormat = NULL;
	char columnString[256] = { 0, };
	char query[QUERYLENGTH] = { 0, };
	if (getColumnString(json_object_get_array(jsonDataObject, "kind"), columnString) == 1) return 1;
	switch (option)
	{
	case All:
		queryFormat = "select %s, if(userkey = %d, \"held\", if(userkey <> %d, if(isCompleted, \"completed\", \"attend\"), \"\"))"
			"as status "
			"from (select event.*, g.iscompleted from event left join (select eventkey, iscompleted from entrylist where userkey = %d) as g on event.eventkey = g.eventkey) as k where userkey = %d or iscompleted is not null";
		sprintf_s(query, QUERYLENGTH, queryFormat, columnString, userData->key, userData->key, userData->key, userData->key);
		break;
	case Attend:
		queryFormat = "select %s "
			"from (select event.eventkey, event.name, event.placekey, event.startDate, event.endDate, event.userkey as hostkey, entrylist.userkey as entryerkey, entrylist.isCompleted "
			"from event left join entrylist on event.eventkey = entrylist.eventkey) as eventlist "
			"where entryerkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, columnString, userData->key);
		break;
	case Completed:
		queryFormat = "select %s "
			"from (select event.eventkey, event.name, event.placekey, event.startDate, event.endDate, event.userkey as hostkey, entrylist.userkey as entryerkey, entrylist.isCompleted "
			"from event left join entrylist on event.eventkey = entrylist.eventkey) as eventlist "
			"where entryerkey = %d and iscompleted = true";
		sprintf_s(query, QUERYLENGTH, queryFormat, columnString, userData->key);
		break;
	case Held:
		queryFormat = "select %s "
			"from event"
			"where userkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, columnString, userData->key);
		break;
	}
	if (limitindexQueryGenerator(json_object_get_value(jsonDataObject, "index"), json_object_get_value(jsonDataObject, "limit"), query) == 1) return 1;
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-FindEvent", query);
#endif 
	if (mysql_query(mysql, query) > 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}


	MYSQL_RES *res = NULL;
	MYSQL_ROW row;
	res = mysql_store_result(mysql);
	my_ulonglong numRow = mysql_num_rows(res);
	my_ulonglong numFiled = mysql_num_fields(res);
	//if (numRow == 0) return 1;
	char ** FieldsName = getFieldNames(res, numFiled);
	int statusIndex = -1;
	for (int i = 0; i < numFiled; i++)
	{
		if (strcmp(FieldsName[i], "status") == 0)
		{
			statusIndex = i;
			break;
		}
	}

	int index = (int)json_object_get_number(jsonDataObject, "index");
	JSON_Value *jsonDataTemp;
	jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STROK);
	const char *lastStatus = NULL;
	if (statusIndex >= 0)
	{
		json_object_set_value(jsonDataObjectTemp, "attend", json_parse_string("[]"));
		json_object_set_value(jsonDataObjectTemp, "completed", json_parse_string("[]"));
		json_object_set_value(jsonDataObjectTemp, "held", json_parse_string("[]"));
	}
	else
	{
		json_object_set_value(jsonDataObjectTemp, "Datas", json_parse_string("[]"));
	}
	for (my_ulonglong i = 0; i < numRow; i++)
	{
		if (json_serialization_size(jsonDataTemp) + 30 >= MAX_BUF)
		{
			if (statusIndex >= 0)
			{
				json_array_remove(json_object_get_array(jsonDataObjectTemp, lastStatus), json_array_get_count(json_object_get_array(jsonDataObjectTemp, lastStatus)) - 1);
			}
			else 
			{
				json_array_remove(json_object_get_array(jsonDataObjectTemp, "Datas"), json_array_get_count(json_object_get_array(jsonDataObjectTemp, "Datas")) - 1);
			}
			if (index)
			{
				json_object_set_number(jsonDataObjectTemp, "index", (double)index);
			}
			else
			{
				json_object_set_number(jsonDataObjectTemp, "index", (double)0);
			}
			json_object_set_number(jsonDataObjectTemp, "limit", (double)i);
			mysql_free_result(res);
			*jsonSize = json_serialization_size(jsonDataTemp);
			json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
			json_value_free(jsonData);
			json_value_free(jsonDataTemp);
			data[*jsonSize - 1] = '\n';
			free(FieldsName);
#ifdef __DEBUG__
			printf("response Data : %s\n", data);
#endif
			return 0;
		}
		row = mysql_fetch_row(res);
		JSON_Value *itemValue;
		itemValue=json_value_init_object();
		JSON_Object *itemObject = json_value_get_object(itemValue);
		for (my_ulonglong j = 0; j < numFiled; j++)
		{
			if (j == statusIndex) continue;
			json_object_set_string(itemObject, FieldsName[j], row[j]);
		}
		if (statusIndex >= 0)
		{
			json_array_append_value(json_object_get_array(jsonDataObjectTemp, row[statusIndex]), itemValue);
			lastStatus = row[statusIndex];
		}
		else
		{
			json_array_append_value(json_object_get_array(jsonDataObjectTemp, "Datas"), itemValue);
		}
	}
	if (index)
	{
		json_object_set_number(jsonDataObjectTemp, "index", (double)index);
	}
	else
	{
		json_object_set_number(jsonDataObjectTemp, "index", (double)0);
	}
	mysql_free_result(res);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
	free(FieldsName);
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

int responseIsCompleted(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	int isCompleted = json_object_get_boolean(jsonDataObject, "iscompleted");
	if (isCompleted == -1) return 1;
	int eventkey = (int)json_object_get_number(jsonDataObject, "key");
	if (eventkey == 0) return 1;
	const char * queryFormat = "call update_entrylist(%d,%d,%d,@result)";  //eventkey, userkey, boolean 
	char query[QUERYLENGTH] = {0,};
	sprintf_s(query, QUERYLENGTH,queryFormat, eventkey, userData->key, isCompleted);
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-IsCompleted", query);
#endif 
	if (mysql_query(mysql, query) != 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}
	memcpy_s(query, QUERYLENGTH, "select @result", sizeof("select @result"));
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-IsCompleted", query);
#endif 
	if (mysql_query(mysql, query) != 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}
	MYSQL_RES *res;
	MYSQL_ROW row;
	res = mysql_store_result(mysql);
	if (res == NULL) return 1;
	row = mysql_fetch_row(res);
	if (atoi(row[0]) == -1) return 1;
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STROK);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

int responseAttend(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	int attend = json_object_get_boolean(jsonDataObject, "attend");
	if (attend == -1) return 1;
	int eventkey = (int)json_object_get_number(jsonDataObject, "key");
	if (eventkey == 0) return 1;
	char query[QUERYLENGTH] = { 0, };
	const char * queryFormat = NULL;
	if (attend)
	{
		queryFormat = "call update_entrylist(%d,%d,%d,@result)";  //eventkey, userkey, boolean 
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey, userData->key, !attend);
	}
	else
	{
		queryFormat = "delete from entrylist where eventkey = %d and userkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey, userData->key);
	}
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-Attend", query);
#endif 
	if (mysql_query(mysql, query) != 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}
	if (attend == 1)
	{
		memcpy_s(query, QUERYLENGTH, "select @result", sizeof("select @result"));
#ifdef __DEBUG__
		printf("%s Query : %s\n-------------------\n", "Entry-IsCompleted", query);
#endif 
		if (mysql_query(mysql, query) != 0)
		{
			printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
			return 1;
		}
		MYSQL_RES *res;
		MYSQL_ROW row;
		res = mysql_store_result(mysql);
		if (res == NULL) return 1;
		row = mysql_fetch_row(res);
		if (atoi(row[0]) == -1) return 1;
	}
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STROK);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

int responseCheckEntry(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	const char *checkEntryOption = json_object_get_string(jsonDataObject, "checkentry");
	if (checkEntryOption == NULL) return 1;
	int eventkey = (int)json_object_get_number(jsonDataObject, "key");
	if (eventkey == 0) return 1;
	EventKeyOption option = getEventKeyOption(checkEntryOption);
	const char * queryFormat = NULL;
	char query[QUERYLENGTH] = { 0, };
	if (option == EventKeyOption_Error) return 1;
	switch (option)
	{
	case All:
		queryFormat = "select numEntry, numCompleted from entry where eventkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
	break;
	case Attend:
		queryFormat = "select numEntry from entry where eventkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
	break;
	case Completed:
		queryFormat = "select numCompleted from entry where eventkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
	break;
	}
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-CheckEntry", query);
#endif 
	if (mysql_query(mysql, query) != 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}
	MYSQL_RES *res;
	MYSQL_ROW row;
	res = mysql_store_result(mysql);
	my_ulonglong numField = mysql_num_fields(res);
	char **fieldNames = NULL;
	fieldNames = getFieldNames(res, numField);
	if (fieldNames == NULL) return 1;
	row = mysql_fetch_row(res);
	JSON_Value *jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STROK);
	json_object_set_value(jsonDataObjectTemp, "Datas", json_parse_string("[]"));
	json_array_append_value(json_object_get_array(jsonDataObjectTemp, "Datas"), json_parse_string("{}"));
	for (my_ulonglong i = 0; i < numField; i++)
	{
		json_object_set_number(
			json_array_get_object(
				json_object_get_array(jsonDataObjectTemp, "Datas"), 0), fieldNames[i], atof(row[i]));
	}
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}
int responseGetEntryList(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData)
{
	if (mysql == NULL || jsonData == NULL || data == NULL || jsonSize == NULL || userData == NULL) return 1;
	JSON_Object *jsonDataObject = json_value_get_object(jsonData);
	const char *getEntryListOption = json_object_get_string(jsonDataObject, "getentrylist");
	if (getEntryListOption == NULL) return 1;
	int eventkey = (int)json_object_get_number(jsonDataObject, "key");
	if (eventkey == 0) return 1;
	EventKeyOption option = getEventKeyOption(getEntryListOption);
	const char * queryFormat = NULL;
	char query[QUERYLENGTH] = { 0, };
	if (option == EventKeyOption_Error) return 1;
	switch (option)
	{
	case All:
		queryFormat = "select nickname, if (iscompleted, \"completed\", \"attend\") as status from (select userlist.nickname, entrylist.iscompleted, entrylist.eventkey from userlist left join entrylist on userlist.userkey = entrylist.userkey) as entryNamelist where eventkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
		break;
	case Attend:
		queryFormat = "select nickname from (select userlist.nickname, entrylist.iscompleted, entrylist.eventkey from userlist left join entrylist on userlist.userkey = entrylist.userkey) as entryNamelist where eventkey = %d";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
		break;
	case Completed:
		queryFormat = "select nickname from (select userlist.nickname, entrylist.iscompleted, entrylist.eventkey from userlist left join entrylist on userlist.userkey = entrylist.userkey) as entryNamelist where eventkey = %d and isCompleted = true";
		sprintf_s(query, QUERYLENGTH, queryFormat, eventkey);
		break;
	}
#ifdef __DEBUG__
	printf("%s Query : %s\n-------------------\n", "Entry-CheckEntry", query);
#endif 
	if (mysql_query(mysql, query) != 0)
	{
		printf("-----------\n""MYSQL Error-----------\n%s\nin %s\n", mysql_error(mysql), getTimetoString());
		return 1;
	}
	MYSQL_RES *res = NULL;
	MYSQL_ROW row;
	res = mysql_store_result(mysql);
	my_ulonglong numRow = mysql_num_rows(res);
	my_ulonglong numFiled = mysql_num_fields(res);
	//if (numRow == 0) return 1;
	char ** FieldsName = getFieldNames(res, numFiled);
	int statusIndex = -1;
	for (int i = 0; i < numFiled; i++)
	{
		if (strcmp(FieldsName[i], "status") == 0)
		{
			statusIndex = i;
			break;
		}
	}

	int index = (int)json_object_get_number(jsonDataObject, "index");
	JSON_Value *jsonDataTemp;
	jsonDataTemp = json_value_init_object();
	JSON_Object *jsonDataObjectTemp = json_value_get_object(jsonDataTemp);
	json_object_set_string(jsonDataObjectTemp, "response", json_object_get_string(jsonDataObject, "request"));
	json_object_set_string(jsonDataObjectTemp, "result", STROK);
	const char *lastStatus = NULL;
	if (statusIndex >= 0)
	{
		json_object_set_value(jsonDataObjectTemp, "attend", json_parse_string("[]"));
		json_object_set_value(jsonDataObjectTemp, "completed", json_parse_string("[]"));
		json_object_set_value(jsonDataObjectTemp, "held", json_parse_string("[]"));
	}
	else
	{
		json_object_set_value(jsonDataObjectTemp, "Datas", json_parse_string("[]"));
	}
	for (my_ulonglong i = 0; i < numRow; i++)
	{
		if (json_serialization_size(jsonDataTemp) + 30 >= MAX_BUF)
		{
			if (statusIndex >= 0)
			{
				json_array_remove(json_object_get_array(jsonDataObjectTemp, lastStatus), json_array_get_count(json_object_get_array(jsonDataObjectTemp, lastStatus)) - 1);
			}
			else
			{
				json_array_remove(json_object_get_array(jsonDataObjectTemp, "Datas"), json_array_get_count(json_object_get_array(jsonDataObjectTemp, "Datas")) - 1);
			}
			if (index)
			{
				json_object_set_number(jsonDataObjectTemp, "index", (double)index);
			}
			else
			{
				json_object_set_number(jsonDataObjectTemp, "index", (double)0);
			}
			json_object_set_number(jsonDataObjectTemp, "limit", (double)i);
			mysql_free_result(res);
			*jsonSize = json_serialization_size(jsonDataTemp);
			json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
			json_value_free(jsonData);
			json_value_free(jsonDataTemp);
			data[*jsonSize - 1] = '\n';
			free(FieldsName);
#ifdef __DEBUG__
			printf("response Data : %s\n", data);
#endif
			return 0;
		}
		row = mysql_fetch_row(res);
		JSON_Value *itemValue;
		itemValue = json_value_init_object();
		JSON_Object *itemObject = json_value_get_object(itemValue);
		for (my_ulonglong j = 0; j < numFiled; j++)
		{
			if (j == statusIndex) continue;
			json_object_set_string(itemObject, FieldsName[j], row[j]);
		}
		if (statusIndex >= 0)
		{
			json_array_append_value(json_object_get_array(jsonDataObjectTemp, row[statusIndex]), itemValue);
			lastStatus = row[statusIndex];
		}
		else
		{
			json_array_append_value(json_object_get_array(jsonDataObjectTemp, "Datas"), itemValue);
		}
	}
	if (index)
	{
		json_object_set_number(jsonDataObjectTemp, "index", (double)index);
	}
	else
	{
		json_object_set_number(jsonDataObjectTemp, "index", (double)0);
	}
	mysql_free_result(res);
	*jsonSize = json_serialization_size(jsonDataTemp);
	json_serialize_to_buffer(jsonDataTemp, data, *jsonSize);
	json_value_free(jsonData);
	json_value_free(jsonDataTemp);
	data[*jsonSize - 1] = '\n';
	free(FieldsName);
#ifdef __DEBUG__
	printf("response Data : %s\n", data);
#endif
	return 0;
}

EventKeyOption getEventKeyOption(const char *option)
{
	if (option == NULL) return EventKeyOption_Error;
	if (strcmp(option, STRALL) == 0) return All;
	if (strcmp(option, STRATTEND) == 0) return Attend;
	if (strcmp(option, STRCOMPLETED) == 0) return Completed;
	if (strcmp(option, STRHELD) == 0) return Held;
	return EventKeyOption_Error;
}

int getColumnString(JSON_Array *kindArray, char *columnString)
{
	if (json_array_get_count(kindArray) == (size_t)0) return 1;
	int columnCount = 0;
	for (size_t i = 0; i < json_array_get_count(kindArray); i++)
	{
		if (json_value_get_type(json_array_get_value(kindArray, i)) != JSONString) continue;
		const char *columnName = json_array_get_string(kindArray, i);
		if (strcmp(columnName, "all") == 0)
		{
			if (strlen(columnString) > 0) memset(columnString, 0, 100);
			sprintf_s(columnString, 100, "eventkey, name, placekey, startDate, endDate");
			return 0;
		}
		if (strcmp(columnName, "eventkey") == 0 || strcmp(columnName, "name") == 0 || 
			strcmp(columnName, "placekey") == 0 || strcmp(columnName, "startDate") == 0 ||
			strcmp(columnName, "endDate") == 0)
		{
			sprintf_s(columnString, 100, "%s %s,", columnString, columnName);
			columnCount++;
		}
	}
	if (columnCount > 0)
	{
		columnString[strlen(columnString) - 1] = '\0';
		return 0;
	}
	else 
	{
		return 1;
	}
}
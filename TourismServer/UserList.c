﻿#include <WinSock2.h>
#include <stdbool.h>
#include <string.h>
#include "UserList.h"
#ifdef __DEBUG__
#include <stdio.h>
#endif

int initUserList(UserList * list)
{
	memset(list, 0, sizeof(UserList));
	return 0;
}

int insertUserData(UserList * list, SOCKET socket, session key, bool isLogin)
{
	if(list == NULL || socket == 0) 
	{
		return 1;
	}
	list->dataArray[list->userCount].socket = socket;
	list->dataArray[list->userCount].key = key;
	list->dataArray[list->userCount].isLogin = isLogin;
	list->userCount++;
	return 0;
}

int deletUserData(UserList * list, int index) 
{
	if (list->userCount == 0) return 1;
	if (list->userCount < index) return 1;
	for (; index < list->userCount ; index++)
	{
		list->dataArray[index] = list->dataArray[index + 1];
	}
	list->userCount--;
	memset(&list->dataArray[list->userCount], 0, sizeof(UserData));
	return 0;
}

int findUserData(UserList * list, const SOCKET socket, const session key)
{
	if (socket == 0 && key == 0)
	{
		return -1;
	}
	for (int i = 0; i < list->userCount; i++)
	{
		if (list->dataArray[i].socket == socket)
		{
#ifdef __DEBUG__ 
			printf("index : %d\n", i);
#endif
			return i;
		}
		if (list->dataArray[i].key == key)
		{
#ifdef __DEBUG__ 
			printf("index : %d\n", i);
#endif
			return i;
		}
	}
	return -1;
}

int setUserData(UserList *list, int index, const SOCKET socket, const session key, const bool isLogin)
{
	if (socket < 0 && key < 0 && isLogin < 0) return 1;
	if (socket != 0) list->dataArray[index].socket = socket;
	if (key != 0) list->dataArray[index].key = key;
	if (isLogin >= 0) list->dataArray[index].isLogin = isLogin;
#ifdef __DEBUG__
	printf("Data ------\n socket : %d\nsession : %d\nisLogin : %d\n", list->dataArray[index].socket, list->dataArray[index].key, list->dataArray[index].isLogin);
#endif
	return 0;
}

const UserData *getUserData(UserList *list, int index)
{
#ifdef __DEBUG__
	printf("Data ------\n socket : %d\nsession : %d\nisLogin : %d\n", list->dataArray[index].socket, list->dataArray[index].key, list->dataArray[index].isLogin);
#endif
	return &(list->dataArray[index]);
}
﻿#include "parson.h"
#include "UserList.h"
#include <WinSock2.h>
#include <mysql.h>
#ifndef DataFormatting
#define DataFormatting

#define STRGET "get"
#define STRPOST "post"
#define STRUPDATE "update"
#define STRDELETE "delete"
#define STRSIGNUP "signup"
#define STRSIGNIN "signin"
#define STRSIGNOUT "signout"
#define STRENTRY "entry"

#define STRAlreadyExist_ID_OR_NICKNAME "exist"
#define STRWRONG_ID_OR_PW "wrong"
#define STRNOT_OK "not_ok"
#define STROK "ok"

#define STRALL "all"
#define STRATTEND "attend"
#define STRCOMPLETED "completed"
#define STRHELD "held"

#define QUERYLENGTH 1024

//프로토콜 정의 
typedef enum _protocolCode ProtocolCode;

enum _protocolCode {
	ProtocolError = -1,   //error
	//Protocol_Response = 0,
	ProtocolRequest = 1  //request
};

typedef enum _requestCode RequestCode;

enum _requestCode {
	Get = 1,
	Post,
	Update,
	Delete,
	SignUp,
	SignIn,
	SignOut,
	Entry,
	Request_Error
};

typedef enum _signProcessingCode SignProcessingCode;

enum _signProcessingCode {
	OK ,
	AlreadyExist_ID_OR_NICKNAME,
	WRONG_ID_OR_PW,
	CommonError,
	UnknownError
};

typedef enum _eventKeyOpton EventKeyOption;
enum _eventkeyOption {
	All,
	Attend,
	Completed,
	Held,
	EventKeyOption_Error
};

#define ISDATA(X) \
((X >= Get) && (X <= Delete))
#define ISSIGN(X) \
((X >= SignUp) && (X <= SignOut))
#define ISENTRY(X) \
(X == Entry)

//json에서 프로토콜 코드 얻기
ProtocolCode getProtocolCode(JSON_Value *jsonMessage);  


//요청문에 대한 응답을 json으로 포맷 한다.
int responseDataFormatting(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, UserList *userList, int index);
//데이터 요청 응답
int responseDataRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, RequestCode Code, const UserData *userData);
//회원 처리 요청 응답
int responseSignRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, RequestCode Code, UserList *list, int index);
//이벤트 참가 관련 응답
int responseEntryRequest(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);


//sql 쿼리문을 생성한다.
int dataQueryGenerator(JSON_Value * jsonData, char *query, RequestCode Code, const UserData *userData); 
//select 쿼리문을 생성한다. 
int selectQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData);
//update 쿼리문을 생성한다.
int updateQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData);
//post 쿼리문을 생성한다.
int postQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData);
//delete 쿼리문을 생성한다.
int deleteQueryGenerator(JSON_Value *jsonData, char *query, const UserData *userData);

//쿼리 결과의 필드들의 이름을 알아온다
char **getFieldNames(MYSQL_RES *res, const my_ulonglong numField);
//request 코드를 얻어 온다
RequestCode getRequestCode(const char * request);
//key에 대한 where문을 생성한다.
int keyWhereQueryGenerator(JSON_Value *key, char *query, const UserData *userData);
//search에 대한 where문을 생성한다.
int searchWhereQueryGenerator(JSON_Array *search, char *query, const UserData *userData);
//index limit에 대한 query문을 생성한다.
int limitindexQueryGenerator(JSON_Value *index, JSON_Value *limit, char *query);

//Get코드로 요청한 데이터를 형식에 맞게 만든다.
int dataFormattingGetCode(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize);
//Delete코드로 요청한 데이터를 형식에 맞게 만든다.
int dataFormattingDeleteCode(JSON_Value *jsonData, char *data, int *jsonSize);
//Post코드로 요청한 데이터를 형식에 맞게 만든다.
int dataFormattingPostCode(MYSQL *mysql,JSON_Value *jsonData, char *data, int *jsonSize);
//Update 코드로 요청한 데이터를 형식에 맞게 만든다.
int dataFormattingUpdateCode(JSON_Value *jsonData, char *data, int *jsonSize);
//클라이언트 접속 처리
int acceptClient(SOCKET serverSocket, fd_set *set, UserList *userList); 
//클라이언트가 접속을 끊은 경우 처리 
int closeClient(SOCKET clientSocket, fd_set *set, UserList *userList);  
//잘못된 json 데이터 응답문 json으로 포맷
void invalidJson(char *message, int *jsonSize);     

//회원 가입
SignProcessingCode responseSignUp(MYSQL *mysql, JSON_Value *jsonData);
//로그인
SignProcessingCode responseSignIn(MYSQL *mysql, JSON_Value *jsonData, UserList *list, int index);
//로그아웃
SignProcessingCode responseSignOut(MYSQL *mysql, JSON_Value *jsonData, UserList *list, int index);

//데이터 응답 에러 처리
int responseError(JSON_Value *jsonData, char *data, int *jsonSize);


//유저와 관련된 이벤트 응답
int responseFindEvent(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);
//이벤트 참가 완료 처리 응답
int responseIsCompleted(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);
//이벤트 참가 처리 응답
int responseAttend(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);
//한 이벤트에 대한 출석표 응답
int responseCheckEntry(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);
//한 이벤트에 참여한 사람들 닉네임 일람
int responseGetEntryList(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize, const UserData *userData);

EventKeyOption getEventKeyOption(const char *option);
int getColumnString(JSON_Array *kindArray, char *columnString);
#endif

//int requestDataFormatting(MYSQL *mysql, JSON_Value *jsonData, char *data, int *jsonSize); 나중에 응답을 요청 json으로 포맷한다.

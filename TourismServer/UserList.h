﻿#ifndef __USER_LIST__
#define __USER_LIST__
#include <WinSock2.h>
#include <stdbool.h>
typedef int session;

typedef struct __userdata 
{
	SOCKET socket;
	session key;
	bool isLogin;
}UserData;
typedef struct __userList 
{
	int userCount;
	UserData dataArray[1024];
} UserList;
//리스트 최기화
int initUserList(UserList * list);
//새로운 유저 추가
int insertUserData(UserList * list, SOCKET socket, session key, bool isLogin);
//유저 삭제
int deletUserData(UserList * list, int index);
//유저 찾기
int findUserData(UserList * list, const SOCKET socket, const session key);
//유저 데이터 수정 socket session에 0을 넣으면 수정하지 않음 그리고 bool은 0미만의 수를 넣으면 수정하지 않음;
int setUserData(UserList *list, int index, const SOCKET socket, const session key, const bool isLogin);
const UserData *getUserData(UserList *list, int index);
#endif
#include <time.h>
static char timeString[256];
static time_t nowTime;
static struct tm formattingTime;

const char* getTimetoString(void)
{
	time(&nowTime);
	localtime_s(&formattingTime, &nowTime);
	strftime(timeString, 256, "%c", &formattingTime);
	return timeString;
}
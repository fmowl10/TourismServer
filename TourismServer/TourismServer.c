﻿#include <WinSock2.h>
#include <mysql.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "parson.h"
#include "DataFormatting.h"
#include "serverData.h"
#include "UserList.h"
#include "getTimetoString.h"


int main(void)
{
	UserList mUserList;
	initUserList(&mUserList);
	system("chcp 65001");
#ifdef __DEBUG__
	printf("DEBUG\n%s\n", getTimetoString());
#else
	printf("RELEASE\n%s\n", getTimetoString());
#endif
	MYSQL * mysqlConn;
	WSADATA ws;
	mysqlConn = mysql_init(NULL);
	if (!mysqlConn)
	{
		printf("mysql_init failed.\n");
		return EXIT_FAILURE;
	}
	if (WSAStartup(MAKEWORD(2, 2), &ws) != 0)
	{
		printf("winsock lib can not init\n");
		return EXIT_FAILURE;
	}
	SOCKET serverSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (serverSocket < 0)
	{
		printf("socket can not init\n");
		WSACleanup();
		return EXIT_FAILURE;
	}
	SOCKADDR_IN serverAddress;
	memset(&serverAddress, 0, sizeof(SOCKADDR_IN));
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORT);
	if ((bind(serverSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR))
	{
		printf("socket bind failed.\n");
		WSACleanup();
		return EXIT_FAILURE;
	}
	my_bool reConnection = true;
	mysql_options(mysqlConn, MYSQL_OPT_RECONNECT, (void*)&reConnection); //자동 재접속 옵션을 킴
	if ((mysqlConn = mysql_real_connect(mysqlConn, HOST, ID, PW, DB, 3306, NULL, CLIENT_INTERACTIVE))) //계속해서 연결을 유지시킨다.
	{
		printf("mysql connection Succeed.\n");
	}
	else
	{
		printf("mysql connection failed.");
		WSACleanup();
		return EXIT_FAILURE;
	}
	printf("listening....\n");
	if (listen(serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		printf("listening is failed\n");
		return EXIT_FAILURE;
	}
	fd_set nowOn, readFd, writeFd;
	FD_ZERO(&nowOn);
	FD_ZERO(&readFd);
	FD_ZERO(&writeFd);
	SOCKET clientSocket;
	int fdNum;
	struct timeval waitTime = { 28700, 0};   //mysql의 연결을 유지하기위한 선언 159줄을 보길
	char message[MAX_BUF] = { 0, };
	JSON_Value * jsonMessage = NULL;
	int jsonSize = 0;
	clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clientSocket < 0)
	{
		printf("fail to client socket generate.\n");
		WSACleanup();
		return EXIT_FAILURE;
	}
	int result;
	unsigned int readStep = 0;
	while (1)
	{
		readFd = nowOn;
		writeFd = nowOn;
		FD_SET(serverSocket, &readFd);
		fdNum = select(0, &readFd, NULL, NULL, NULL);
		if (fdNum)
		{
			for (readStep = 0; readStep < readFd.fd_count; readStep++)
			{
				if (FD_ISSET(serverSocket, &readFd) && serverSocket == readFd.fd_array[readStep])
				{
					if ((result = acceptClient(serverSocket, &nowOn, &mUserList)) == 1)
					{
						printf("There was a problem with the client connection.\n");
					}
					else
					{
						printf("client %d's connection was success. in %s\n", result, getTimetoString());
					}
					continue;
				}
				if (FD_ISSET(readFd.fd_array[readStep], &readFd))
				{
					clientSocket = readFd.fd_array[readStep];
					result = recv(clientSocket, message, MAX_BUF, 0);
					if (result <= 0)
					{
						result = closeClient(clientSocket, &nowOn, &mUserList);
						if (result == 0)
						{
							printf("client %d's disconnection was success. in %s\n", clientSocket, getTimetoString());
						}
						continue;
					}
#ifdef __DEBUG__
					printf("client message : %s\nRequsted in : %s\n", message, getTimetoString());
#else
					printf("client Requseted in : %s\n", getTimetoString());
#endif // __DEBUG__

					jsonMessage = json_parse_string(message);
					memset(message, 0, MAX_BUF);
					switch (getProtocolCode(jsonMessage))
					{
					case ProtocolError:
						json_value_free(jsonMessage);
						invalidJson(message, &jsonSize);
						send(clientSocket, message, jsonSize, 0); //json으로 에러 메세지를 보낸다.
						break;
					case ProtocolRequest:
						responseDataFormatting(mysqlConn, jsonMessage, message, &jsonSize, &mUserList, findUserData(&mUserList, clientSocket, 0));
#ifdef __DEBUG__
						printf("Sending Data ----------------------------- \n%s\nLength : %d\nReqponse in : %s\n", message, jsonSize, getTimetoString());
#else
						printf("Response in : %s\n", getTimetoString());
#endif // __DEBUG__

						send(clientSocket, message, jsonSize, 0);  //json으로 포맷된 데이터를 보낸다.
						break;
					}
				}
			}
			continue;
		} 
		else
		{
			mysql_query(mysqlConn, "select @result");     //mysql과의 연결이 28800초(8시간)이 지나면 자동으로 끊기므로 timeval에서 100초 전에 실행하도록 해놓아서 mysql과의 연결을 지속시킴
		}
	}

	system("PAUSE");
	return EXIT_FAILURE;
}

